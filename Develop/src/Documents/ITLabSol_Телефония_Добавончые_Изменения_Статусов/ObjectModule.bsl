
Процедура ОбработкаПроведения(Отказ, Режим)
	//{{__КОНСТРУКТОР_ДВИЖЕНИЙ_РЕГИСТРОВ
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!!!

	// регистр ITLabSol_Телефония_Добавончые_Статус
	Движения.ITLabSol_Телефония_Добавончые_Статус.Записывать = Истина;
	Движение = Движения.ITLabSol_Телефония_Добавончые_Статус.Добавить();
	Движение.Период = Дата;
	Движение.СсылкаНаСущность = СсылкаНаСущность;
	Движение.Статус = Статус;

	//}}__КОНСТРУКТОР_ДВИЖЕНИЙ_РЕГИСТРОВ
КонецПроцедуры
