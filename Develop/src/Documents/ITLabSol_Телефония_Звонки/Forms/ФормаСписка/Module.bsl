
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ИндексВн = 2;
	ИндексВх = 1;
	ИндексИсх = 0;
КонецПроцедуры

&НаСервере
Функция ПолучитьперечислениеНаправления(Направление)
	Если Направление = "ВН" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Направления.Внутренний;
	ИначеЕсли Направление = "ИСХ" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Направления.Исходящий;
	ИначеЕсли Направление = "ВХ" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Направления.Входящий;
	КонецЕсли
КонецФункции
&НаСервере
Функция Получитьперечислениерезультата(результат)
	Если результат = "УСП" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Результаты.Успешный;
	ИначеЕсли результат = "!УСП" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Результаты.Неуспешный;
	ИначеЕсли результат = "ПРОП" Тогда
		Возврат Перечисления.ITLabSol_Телефония_Звонки_Результаты.Пропущенный;
	КонецЕсли
КонецФункции

&НаКлиенте
Процедура ПриИзмененииОтбора(Элемент, Текст = "", СтандартнаяОбработка = Неопределено)
	Список.Отбор.Элементы.Очистить();
	ГруппаОтбораНаправления = Список.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбораНаправления.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИЛИ;
	
	ГруппаОтбораРезультата = Список.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
	ГруппаОтбораРезультата.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИЛИ;
	
	Если Внутренние = Истина Тогда
		ОтборЭлементов = ГруппаОтбораНаправления.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Направление");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = ПолучитьперечислениеНаправления("ВН");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	Если Исходящие = Истина Тогда
		ОтборЭлементов = ГруппаОтбораНаправления.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Направление");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = ПолучитьперечислениеНаправления("ИСХ");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	Если Входящие = Истина Тогда
		ОтборЭлементов = ГруппаОтбораНаправления.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Направление");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = ПолучитьперечислениеНаправления("ВХ");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	
	Если Успешные = Истина Тогда
		ОтборЭлементов = ГруппаОтбораРезультата.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Результат");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = Получитьперечислениерезультата("УСП");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	Если Неуспешные = Истина Тогда
		ОтборЭлементов = ГруппаОтбораРезультата.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Результат");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = Получитьперечислениерезультата("!УСП");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	Если Пропущенные = Истина Тогда
		ОтборЭлементов = ГруппаОтбораРезультата.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Результат");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ОтборЭлементов.ПравоеЗначение = Получитьперечислениерезультата("ПРОП");
		ОтборЭлементов.Использование = Истина;
	КонецЕсли;
	
	Если НЕ ПустаяСТРОКА(ПоискПоНомеру) Тогда
		ГруппаОтбораНомера = Список.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
		ГруппаОтбораНомера.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли;
		
		ОтборЭлементов = ГруппаОтбораНомера.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Источник");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Содержит;
		ОтборЭлементов.ПравоеЗначение = ПоискПоНомеру;
		ОтборЭлементов.Использование = Истина;
		
		ОтборЭлементов = ГруппаОтбораНомера.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Назначение");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Содержит;
		ОтборЭлементов.ПравоеЗначение = ПоискПоНомеру;
		ОтборЭлементов.Использование = Истина;
	Конецесли;
КонецПроцедуры

&НаКлиенте
Процедура СбросОтбора(Команда)
	Список.Отбор.Элементы.Очистить();
	Пропущенные = Ложь;
	Неуспешные = Ложь;
	Успешные = Ложь;
	Входящие = Ложь;
	Исходящие = Ложь;
	Внутренние = Ложь;
	ПоискПоНомеру = "";
КонецПроцедуры

&НаКлиенте
Процедура Поиск(Команда)
	Если НЕ ПустаяСТРОКА(ПоискПоНомеру) Тогда
		ГруппаОтбораНомера = Список.Отбор.Элементы.Добавить(Тип("ГруппаЭлементовОтбораКомпоновкиДанных"));
		ГруппаОтбораНомера.ТипГруппы = ТипГруппыЭлементовОтбораКомпоновкиДанных.ГруппаИли;
		
		ОтборЭлементов = ГруппаОтбораНомера.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Источник");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Содержит;
		ОтборЭлементов.ПравоеЗначение = ПоискПоНомеру;
		ОтборЭлементов.Использование = Истина;
		
		ОтборЭлементов = ГруппаОтбораНомера.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ОтборЭлементов.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Назначение");
		ОтборЭлементов.ВидСравнения = ВидСравненияКомпоновкиДанных.Содержит;
		ОтборЭлементов.ПравоеЗначение = ПоискПоНомеру;
		ОтборЭлементов.Использование = Истина;
	Конецесли;
КонецПроцедуры
