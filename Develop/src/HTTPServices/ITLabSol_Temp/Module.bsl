
Функция ПолучитьСотрудника(НаименованиеСотрудника)
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	Сотрудники.Наименование КАК Наименование,
		|	Сотрудники.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Сотрудники КАК Сотрудники
		|ГДЕ
		|	Сотрудники.Наименование = &Наименование";
	
	Запрос.УстановитьПараметр("Наименование", НаименованиеСотрудника);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.Ссылка;
	КонецЦикла;

	Возврат Неопределено;
КонецФункции

Функция ПолучитьУникальныйИдентификаторАТС()
	//{{КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!!!
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ITLabSol_Телефония_СписокАТС.Общие_УникальныйИдентификаторАТС КАК Общие_УникальныйИдентификаторАТС
		|ИЗ
		|	Документ.ITLabSol_Телефония_СписокАТС КАК ITLabSol_Телефония_СписокАТС";
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.Общие_УникальныйИдентификаторАТС;
	КонецЦикла;
	
	//}}КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА

	Возврат Неопределено;
КонецФункции

Функция setSET(Запрос)
	ТелоЗапроса = РаскодироватьСтроку(Запрос.ПолучитьТелоКакСтроку(), СпособКодированияСтроки.КодировкаURL);
	Попытка
		Объект = ITLabSol_JSON.Сериализовать(ТелоЗапроса);
		Сотрудник = ПолучитьСотрудника(Объект.username);
		СсылкаНаСущность = ITLabSol_Телефония_Сущности.НайтиВнутреннююСущностьИВернутьСсылку(
			ПолучитьУникальныйИдентификаторАТС(), 
			Объект.extension, 
			Перечисления.ITLabSol_Телефония_Сущности_Тип.Номер
		); 
		Если СсылкаНаСущность <> Неопределено Тогда
			Если Сотрудник = Неопределено Тогда
				ITLabSol_Телефония_Операторы.Сбросить(СсылкаНаСущность);		
			Иначе
				ITLabSol_Телефония_Операторы.Назначить(Сотрудник, СсылкаНаСущность);
			КонецЕсли 
		КонецЕсли;
	Исключение
	КонецПопытки;
	Ответ = Новый HTTPСервисОтвет(200);
	Возврат Ответ;
КонецФункции
