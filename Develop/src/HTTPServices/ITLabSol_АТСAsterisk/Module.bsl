Функция ПолучитьИсточникИфнормационногоСообщения()
	Возврат Перечисления.ITLabSol_Система_Сообщения_Источники.АТСAsterisk_HTTP_Сервис;
КонецФункции
Процедура СистемноеСообщение(Сообщение = "", Уровень = Неопределено)
	ITLabSol_Система_Сервер.ЗаписатьСистемноеСообщение(
			ПолучитьИсточникИфнормационногоСообщения(),
			Уровень,
			Сообщение
	);
КонецПроцедуры

Функция all_eventsPOST(Запрос)
	ТелоЗапроса = РаскодироватьСтроку(Запрос.ПолучитьТелоКакСтроку(), СпособКодированияСтроки.КодировкаURL);
	СистемноеСообщение(ТелоЗапроса, Перечисления.ITLabSol_Система_Сообщения_Уровни.Разработка);
	Параметры = Новый Массив();
	Параметры.Добавить(ITLabSol_JSON.Сериализовать(ТелоЗапроса));
	Попытка
		ITLabSol_Система_Сервер.ЗапуститьВФонеИначеСинхронно("ITLabSol_Телефония_АТСAsterisk_Events.ОбработатьСобытие", Параметры);
	Исключение
		СистемноеСообщение(ОписаниеОшибки());
		СистемноеСообщение("Модуль расширения Телефонии Астериск не найден");
	КонецПопытки;
	Ответ = Новый HTTPСервисОтвет(200);
	Возврат Ответ;
КонецФункции
