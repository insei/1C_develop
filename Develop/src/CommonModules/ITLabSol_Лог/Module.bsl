&НаСервере
Процедура ЗаписатьСинхронно(ИсточникПеречисление, УровеньПеречисление = неопределено, Сообщение="") Экспорт
	РегистрыСведений.ITLabSol_Система_Логи.Записать(ИсточникПеречисление, УровеньПеречисление, Сообщение);
КонецПроцедуры

// Процедура - Записать - Записывает сообщение в лог.
//
// Параметры:
//  Источник	 - Строка - Источник информационного сообщения, строка или тип данных имеющий строковое представление
//  Сообщение	 - Строка - Сообщение.
//  Уровень		 - Строка,Перечисление - 
//		Уровень информационногосообщения: 
//			"E" или "Error", "W" или "Warning", "I" или "Info" или "D" или "Debug". 
//			"О" или "Ошибка", "П" или "Предупреждение", "И" или "Информация", "Р" - "Разработка".
Процедура Записать(Источник, Сообщение, Уровень = "Разработка") Экспорт
	ПараментрыЗапуска = Новый Массив();
	ПараментрыЗапуска.Добавить(Источник);
	ПараментрыЗапуска.Добавить(Уровень);
	ПараментрыЗапуска.Добавить(Сообщение);
	ITLabSol_Система_Сервер.ЗапуститьВФонеИначеСинхронно("ITLabSol_Лог.ЗаписатьСинхронно", ПараментрыЗапуска, , "Асинхронная запись сообщения в лог");
КонецПроцедуры