Функция ПолучитьСтруктуруСообщения(Номер = "", Текст = "") экспорт
	СтруктураСообщения = Новый Структура();
	СтруктураСообщения.Вставить("ИД", "");
	СтруктураСообщения.Вставить("Номер", Номер);
	СтруктураСообщения.Вставить("Текст", Текст);
	СтруктураСообщения.Вставить("ИмяОтправителя", "");
	СтруктураСообщения.Вставить("Ссылки", Новый Структура());
	СтруктураСообщения.Ссылки.Вставить("Рассылка");
	СтруктураСообщения.Ссылки.Вставить("Сообщение");
	СтруктураСообщения.Ссылки.Вставить("Сервис");
	СтруктураСообщения.Вставить("Статус");
	Возврат СтруктураСообщения;
КонецФункции

Функция ПолучитьСтатус(СсылкаНаСМССообщение) экспорт
	//{{КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ITLabSol_СМС_СтатусыСообщенийСрезПоследних.Статус
		|ИЗ
		|	РегистрСведений.ITLabSol_СМС_СтатусыСообщений.СрезПоследних КАК ITLabSol_СМС_СтатусыСообщенийСрезПоследних
		|ГДЕ
		|	ITLabSol_СМС_СтатусыСообщенийСрезПоследних.СсылкаНаСМССообщение = &СсылкаНаСМССообщение";
	
	Запрос.УстановитьПараметр("СсылкаНаСМССообщение", СсылкаНаСМССообщение);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.Статус;
	КонецЦикла;
	
	//}}КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	Возврат Неопределено;
КонецФункции

Функция НайтиСообщениеРассылки(СсылкаНаРассылку, НомерТелефона) экспорт
	//{{КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	// Данный фрагмент построен конструктором.
	// При повторном использовании конструктора, внесенные вручную изменения будут утеряны!
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ITLabSol_СМС_Сообщения.Ссылка
		|ИЗ
		|	Документ.ITLabSol_СМС_Сообщения КАК ITLabSol_СМС_Сообщения
		|ГДЕ
		|	ITLabSol_СМС_Сообщения.СсылкаНаРассылку = &СсылкаНаРассылку
		|	И ITLabSol_СМС_Сообщения.НомерТелефона = &НомерТелефона";
	
	Запрос.УстановитьПараметр("НомерТелефона", НомерТелефона);
	Запрос.УстановитьПараметр("СсылкаНаРассылку", СсылкаНаРассылку);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.Ссылка;
	КонецЦикла;
	
	//}}КОНСТРУКТОР_ЗАПРОСА_С_ОБРАБОТКОЙ_РЕЗУЛЬТАТА
	Возврат Неопределено;
КонецФункции

Процедура ЗаписатьСтатус(СсылкаНаСМССообщение, СтатусОтправкиСообщ) экспорт
	СтатусСМССообщения = РегистрыСведений.ITLabSol_СМС_СтатусыСообщений.СоздатьМенеджерЗаписи();
	СтатусСМССообщения.СсылкаНаСМССообщение = СсылкаНаСМССообщение;
	СтатусСМССообщения.Статус = СтатусОтправкиСообщ;
	СтатусСМССообщения.Период = ТекущаяДата();
	СтатусСМССообщения.Записать();
КонецПроцедуры

Функция ОбновитьСтатус(СсылкаНаСМССообщение) экспорт
	Если СсылкаНаСМССообщение.СсылкаНаСервисСМСОтправки.ТипСМССервиса = Перечисления.ITLabSol_СМС_СервисыОтправки_Тип.DigitalDirect Тогда
		Статус = ITLabSol_СМС_DD_API.ПолучитьСтатусСообщения(СсылкаНаСМССообщение.СсылкаНаСервисСМСОтправки, СсылкаНаСМССообщение.ИДОтправки);
	ИначеЕсли СсылкаНаСМССообщение.СсылкаНаСервисСМСОтправки.ТипСМССервиса = Перечисления.ITLabSol_СМС_СервисыОтправки_Тип.MainSMS Тогда
		Статус = ITLabSol_СМС_MainSMS_API.ПолучитьСтатусСообщения(СсылкаНаСМССообщение.СсылкаНаСервисСМСОтправки, СсылкаНаСМССообщение.ИДОтправки);
	КонецЕсли;
	Если Статус <> Неопределено И Статус <> ПолучитьСтатус(СсылкаНаСМССообщение) Тогда
		ЗаписатьСтатус(СсылкаНаСМССообщение, Статус);
	КонецЕсли;
	Возврат Статус;
КонецФункции

Процедура Записать(СтруктураСообщения) экспорт
	ДокументСообщение = Документы.ITLabSol_СМС_Сообщения.СоздатьДокумент();
	ДокументСообщение.Сообщение = СтруктураСообщения.Текст;
	ДокументСообщение.Дата = ТекущаяДата();
	ДокументСообщение.НомерТелефона = СтруктураСообщения.Номер;
	ДокументСообщение.СсылкаНаРассылку = СтруктураСообщения.Ссылки.Рассылка;
	ДокументСообщение.СсылкаНаСервисСМСОтправки = СтруктураСообщения.Ссылки.Сервис;
	ДокументСообщение.ИмяОтправителя = СтруктураСообщения.ИмяОтправителя;
	ДокументСообщение.ИДОтправки = СтруктураСообщения.ИД;
	ДокументСообщение.Записать();
	ЗаписатьСтатус(ДокументСообщение.Ссылка, СтруктураСообщения.Статус);
	СтруктураСообщения.Ссылки.Сообщение = ДокументСообщение.Ссылка;
КонецПроцедуры

Процедура Отправить(СтруктураСообщения) экспорт
	СтруктураСообщения.Номер = ITLabSol.ПривестиНомерТелефона(СтруктураСообщения.Номер);
	Если СтруктураСообщения.Ссылки.Сервис.ТипСМССервиса = Перечисления.ITLabSol_СМС_СервисыОтправки_Тип.DigitalDirect Тогда
		ITLabSol_СМС_DD_API.ОтправитьСообщение(СтруктураСообщения);
	ИначеЕсли СтруктураСообщения.Ссылки.Сервис.ТипСМССервиса = Перечисления.ITLabSol_СМС_СервисыОтправки_Тип.MainSMS Тогда
		ITLabSol_СМС_MainSMS_API.ОтправитьСообщение(СтруктураСообщения);
	КонецЕсли;
КонецПроцедуры

Процедура ОтправитьИЗаписать(СтруктураСообщения) экспорт
	Отправить(СтруктураСообщения);
	Записать(СтруктураСообщения);
КонецПроцедуры
