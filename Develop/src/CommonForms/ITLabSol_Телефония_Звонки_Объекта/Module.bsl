
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	МассивСсылокОбъектов = Новый Массив();
	Для каждого ЭлементСписка из Параметры.СписокСсылокОбъектов Цикл
		МассивСсылокОбъектов.Добавить(ЭлементСписка.Значение);
	КонецЦикла;
	Звонки.Параметры.УстановитьЗначениеПараметра("МассивСсылокОбъектов", МассивСсылокОбъектов);
КонецПроцедуры
